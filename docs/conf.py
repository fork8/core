project = "QuantumCore"
copyright = "2020, QuantumCore contributors"
version = "dev"
release = "dev"

extensions = [
    'breathe',
    'exhale'
]

breathe_projects = {
    "QuantumCore": "./doxyoutput/xml"
}
breathe_default_project = "QuantumCore"

exhale_args = {
    # These arguments are required
    "containmentFolder":     "./api",
    "rootFileName":          "library_root.rst",
    "rootFileTitle":         "Library API",
    "doxygenStripFromPath":  "../src",
    # Suggested optional arguments
    "createTreeView":        True,
    # TIP: if using the sphinx-bootstrap-theme, you need
    # "treeViewIsBootstrap": True,
    "exhaleExecutesDoxygen": True,
    "exhaleDoxygenStdin":    "INPUT = ../src"
}

primary_domain = 'cpp'
highlight_language = 'cpp'

html_theme = "sphinx_rtd_theme"
html_theme_options = {

}