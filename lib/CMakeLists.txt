include("libbcrypt.cmake")
add_subdirectory(packets-compiler)

add_library(bredis INTERFACE)
target_include_directories(bredis INTERFACE "bredis/include/")

add_subdirectory(lzokay)

add_library(cpptoml INTERFACE)
target_include_directories(cpptoml INTERFACE "cpptoml/include/")