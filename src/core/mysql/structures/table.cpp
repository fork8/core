// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "table.hpp"

#include <algorithm>
#include <stdexcept>
#include <utility>

namespace core::mysql::structures {
    Table::Builder::Builder(std::string database, std::string name)
        : _database(std::move(database)), _name(std::move(name)), _columns() {}

    Table::Builder &Table::Builder::operator<<(const Column &column) {
        auto res = std::find_if(_columns.begin(), _columns.end(),
                                [&column](const Column &c) {
                                    return column.GetName() == c.GetName();
                                });
        if (res == _columns.end()) {
            _columns.push_back(column);
            return *this;
        }

        throw std::invalid_argument("Column name already exists");
    }

    Table::Table(std::string database, std::string name,
                 std::vector<Column> columns)
        : _database(std::move(database)),
          _name(std::move(name)),
          _columns(std::move(columns)) {}

    const std::string &Table::GetDatabase() const { return _database; }

    const std::string &Table::GetName() const { return _name; }

    const std::vector<Column> &Table::GetColumns() { return _columns; }
}  // namespace core::mysql::structures