// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "scripting_engine.hpp"

#include <utility>

namespace core::scripting {
    ScriptingEngineAbstract::ScriptingEngineAbstract(std::string name)
        : _name(std::move(name)) {}

    ScriptingEngineAbstract::~ScriptingEngineAbstract() = default;

    const std::string &ScriptingEngineAbstract::GetName() { return _name; }
}  // namespace core::scripting
