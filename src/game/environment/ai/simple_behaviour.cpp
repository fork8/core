// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#include "simple_behaviour.hpp"

#include <utility>

#include "../../../core/utils/math.hpp"
#include "../../application.hpp"

namespace game::environment::ai {
    SimpleBehaviour::SimpleBehaviour(std::shared_ptr<Monster> &&monster)
        : _monster(std::move(monster)),
          _startX(0),
          _startY(0),
          _targetX(0),
          _targetY(0),
          _movementStart(0),
          _movementDuration(0),
          _nextMovementIn(INT_MAX) {}

    SimpleBehaviour::~SimpleBehaviour() = default;

    void SimpleBehaviour::Init() { CalculateNextMovement(); }

    void SimpleBehaviour::Update(uint32_t elapsedTime) {
        auto time =
            Application::GetInstance()->GetCoreApplication()->GetCoreTime();

        if (_state == IDLE) {
            _nextMovementIn -= elapsedTime;
            if (_nextMovementIn <= 0) {
                // Calculate move target
                if (RandomTarget()) {
                    // Enable moving state
                    _state = MOVING;

                    // Send "WAIT" packet to players
                    SendMovePacket();

                    CalculateNextMovement();
                }
            }
        }

        if (_state == MOVING) {
            auto elapsed = time - _movementStart;
            auto rate = elapsed / (float)_movementDuration;
            if (rate > 1) rate = 1;

            auto x = (int32_t)((float)(_targetX - _startX) * rate + _startX);
            auto y = (int32_t)((float)(_targetY - _startY) * rate + _startY);

            _monster->SetPositionX(x);
            _monster->SetPositionY(y);

            if (rate >= 1) {
                _state = IDLE;
            }
        }
    }

    void SimpleBehaviour::CalculateNextMovement() {
        _nextMovementIn = Application::GetInstance()
                              ->GetCoreApplication()
                              ->GetRandomNumber<unsigned short>(10000, 20000);
    }

    bool SimpleBehaviour::RandomTarget() {
        const uint16_t moveRadius = 1000;

        auto game = Application::GetInstance();
        auto core = game->GetCoreApplication();

        // Get animation data for run
        auto animation = game->GetAnimationManager()->GetAnimation(
            _monster->GetProto().id, formats::AnimationType::RUN,
            formats::AnimationSubType::GENERAL);
        if (!animation) {
            CORE_LOGGING(warning)
                << "Can't move monster, no run animation found "
                << _monster->GetProto().id;
            return false;
        }

        // Choose random location around spawn position
        auto offsetX = core->GetRandomNumber<short>(-moveRadius, moveRadius);
        auto offsetY = core->GetRandomNumber<short>(-moveRadius, moveRadius);
        _startX = _monster->GetPositionX();
        _startY = _monster->GetPositionY();
        _targetX = _monster->GetSpawnX() + offsetX;
        _targetY = _monster->GetSpawnY() + offsetY;

        // Calculate rotation to target
        auto rotation = core::utils::Math::CalculateRotation(
            static_cast<float>(offsetX), static_cast<float>(offsetY));
        if (rotation < 0) {
            rotation += 360;
        }
        _monster->SetRotation(rotation);

        // Calculate movement duration
        float distance =
            core::utils::Math::Distance(_startX, _startY, _targetX, _targetY);
        float animationSpeed =
            -animation->accumulationY / animation->motionDuration;

        int i = 100 - _monster->GetProto().moveSpeed;
        if (i > 0) {
            i = 100 + i;
        } else if (i < 0) {
            i = 10000 / (100 - i);
        } else {
            i = 100;
        }
        int duration =
            static_cast<int>((distance / animationSpeed) * 1000) * i / 100;

        _movementStart = core->GetCoreTime();
        _movementDuration = duration;

        return true;
    }

    void SimpleBehaviour::SendMovePacket() {
        auto movePacket = Application::GetInstance()
                              ->GetServer()
                              ->GetPacketManager()
                              ->CreatePacket(0x03);
        movePacket->SetField<uint8_t>("movementType", MovementTypes::WAIT);
        movePacket->SetField<uint8_t>(
            "rotation", static_cast<uint8_t>(_monster->GetRotation() / 5));
        movePacket->SetField<uint32_t>("vid", _monster->GetVID());
        movePacket->SetField<int32_t>("x", _targetX);
        movePacket->SetField<int32_t>("y", _targetY);
        movePacket->SetField<uint32_t>(
            "time",
            Application::GetInstance()->GetCoreApplication()->GetCoreTime());
        movePacket->SetField<uint32_t>("duration", _movementDuration);

        _monster->SendPacketAround(movePacket);
    }

    void SimpleBehaviour::ObjectEnteredView(
        const std::shared_ptr<Object> &object) {
        if (object->GetObjectType() != PLAYER) return;
        if (_state != MOVING) return;

        auto player = std::static_pointer_cast<Player>(object);

        auto movePacket = Application::GetInstance()
                              ->GetServer()
                              ->GetPacketManager()
                              ->CreatePacket(0x03);
        movePacket->SetField<uint8_t>("movementType", MovementTypes::WAIT);
        movePacket->SetField<uint8_t>(
            "rotation", static_cast<uint8_t>(_monster->GetRotation() / 5));
        movePacket->SetField<uint32_t>("vid", _monster->GetVID());
        movePacket->SetField<int32_t>("x", _targetX);
        movePacket->SetField<int32_t>("y", _targetY);
        movePacket->SetField<uint32_t>(
            "time",
            Application::GetInstance()->GetCoreApplication()->GetCoreTime());
        movePacket->SetField<uint32_t>(
            "duration", _movementDuration);  // is ignored by the server

        player->GetConnection()->Send(movePacket);
    }
}  // namespace game::environment::ai