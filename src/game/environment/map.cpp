// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "map.hpp"

#include <utility>

#include "../../core/logger.hpp"
#include "../../core/networking/server.hpp"
#include "../../core/profiler.hpp"
#include "../application.hpp"
#include "player.hpp"

namespace game::environment {
    Map::Map(std::string name, uint64_t x, uint64_t y, uint8_t width,
             uint8_t height, std::shared_ptr<World> world)
        : _name(std::move(name)),
          _x(x),
          _y(y),
          _width(width),
          _height(height),
          _world(std::move(world)),
          _spawnData(),
          _objects(),
          _quadTree(x, y, width * UNIT_SIZE, height * UNIT_SIZE) {
        CORE_LOGGING(trace)
            << "Creating map " << _name << " at " << x << "," << y << " ("
            << (int)width << "x" << (int)height << ")";
    }
    Map::~Map() = default;

    bool Map::Load() {
        bool ret = true;

        try {
            // Load spawn details
            auto spawnConfig =
                cpptoml::parse_file("data/maps/" + _name + "/spawn.toml");

            auto spawnArray = spawnConfig->get_table_array("spawn");
            for (const auto &spawn : *spawnArray) {
                SpawnData spawnData;
                spawnData.Load(spawn);

                _spawnData.push_back(std::move(spawnData));
            }
        } catch (const cpptoml::parse_exception &e) {
            CORE_LOGGING(warning) << "Failed to load spawn data for map "
                                  << _name << ": " << e.what();
            ret = false;
        }

        return ret;
    }

    void Map::InitialSpawn() {
        for (auto &spawn : _spawnData) {
            spawn.Spawn(shared_from_this());
        }
    }

    void Map::Update(uint32_t elapsedTime) {
        {
            std::lock_guard<std::mutex> lock(_spawnMutex);

            while (!_spawnQueue.empty()) {
                auto &object = _spawnQueue.front();

                if (!_quadTree.Insert(object)) {
                    CORE_LOGGING(error)
                        << "Failed to insert object into root quad tree!";
                    continue;
                }

                object->OnSpawned();

                _objects[object->GetVID()] = object;

                _spawnQueue.pop();
            }
        }

        {
            std::lock_guard<std::mutex> lock(_despawnMutex);

            while (!_despawnQueue.empty()) {
                auto &object = _despawnQueue.front();

                _quadTree.Remove(object);

                std::vector<std::shared_ptr<Object>> objects;
                _quadTree.QueryObjectsAround(objects, object->GetPositionX(),
                                             object->GetPositionY(), 10000);

                for (auto &qobject : objects) {
                    if (qobject->GetObjectType() != PLAYER) continue;

                    std::shared_ptr<Player> player =
                        std::static_pointer_cast<Player>(qobject);
                    object->Remove(player->GetConnection());
                }

                _objects.erase(object->GetVID());

                _despawnQueue.pop();
            }
        }

        for (const auto &object : _objects) {
            object.second->Update(elapsedTime);

            if (object.second->GetPositionModified()) {
                _quadTree.Remove(object.second);
                _quadTree.Insert(object.second);

                object.second->SetPositionModified(false);
            }
        }
    }

    void Map::SpawnObject(const std::shared_ptr<Object> &object) {
        std::lock_guard<std::mutex> lock(_spawnMutex);

        _spawnQueue.push(object);
    }

    void Map::DespawnObject(const std::shared_ptr<Object> &object) {
        std::lock_guard<std::mutex> lock(_despawnMutex);

        _despawnQueue.push(object);
    }

    void Map::QueryObjectsAround(std::vector<std::shared_ptr<Object>> &out,
                                 uint64_t x, uint64_t y, uint32_t radius) {
        _quadTree.QueryObjectsAround(out, x, y, radius);
    }

    SpawnData::SpawnData()
        : _type(SpawnData::SPAWN_GROUP),
          _x(0),
          _y(0),
          _range(0),
          _respawnTime(15) {}

    SpawnData::~SpawnData() = default;

    void SpawnData::Load(const std::shared_ptr<cpptoml::table> &config) {
        auto type = config->get_as<std::string>("type");
        if (type) {
            if (*type == "group")
                _type = SpawnData::SPAWN_GROUP;
            else if (*type == "monster")
                _type = SpawnData::SPAWN_MONSTER;
            else {
                CORE_LOGGING(warning)
                    << "Invalid spawn type " << *type << " using default type";
            }
        }

        auto x = config->get_as<int32_t>("x");
        if (x) _x = *x;

        auto y = config->get_as<int32_t>("y");
        if (y) _y = *y;

        auto range = config->get_as<uint32_t>("range");
        if (range) _range = *range;

        auto respawnTime = config->get_as<uint64_t>("respawnTime");
        if (respawnTime) _respawnTime = *respawnTime;

        switch (_type) {
            case SpawnData::SPAWN_GROUP: {
                auto arr = config->get_array_of<int64_t>("groups");
                for (const auto &id : *arr) {
                    _groups.push_back(id);
                }
                break;
            }
            case SpawnData::SPAWN_MONSTER: {
                auto arr = config->get_array_of<int64_t>("monsters");
                for (const auto &id : *arr) {
                    _monsters.push_back(static_cast<uint32_t>(id));
                }
                break;
            }
        }
    }

    void SpawnData::Spawn(const std::shared_ptr<Map> &map) {
        if (_type == SpawnData::SPAWN_MONSTER) {
            if (_monsters.empty()) return;

            auto core = Application::GetInstance()->GetCoreApplication();

            auto idx = core->GetRandomNumber<size_t>(0, _monsters.size() - 1);
            auto monsterId = _monsters[idx];

            auto x = _x + core->GetRandomNumber<int32_t>(
                              -static_cast<int32_t>(_range), _range);
            auto y = _y + core->GetRandomNumber<int32_t>(
                              -static_cast<int32_t>(_range), _range);
            auto rotation = core->GetRandomNumber<short>(0, 360);

            auto monster = map->_world->CreateMonster(
                monsterId, map->_x + x * 100, map->_y + y * 100, rotation);
            map->_world->SpawnObject(monster);
            _instances[monster->GetVID()] = std::move(monster);
        } else if (_type == SpawnData::SPAWN_GROUP) {
            if (_groups.empty()) return;

            auto core = Application::GetInstance()->GetCoreApplication();

            auto idx = core->GetRandomNumber<size_t>(0, _groups.size() - 1);
            auto groupId = _groups[idx];

            auto group = map->_world->GetSpawnGroup(groupId);

            auto x = _x + core->GetRandomNumber<int32_t>(
                              -static_cast<int32_t>(_range), _range);
            auto y = _y + core->GetRandomNumber<int32_t>(
                              -static_cast<int32_t>(_range), _range);

            for (const auto &monsterId : group.members) {
                auto rotation = core->GetRandomNumber<short>(0, 360);

                auto monster = map->_world->CreateMonster(
                    monsterId, map->_x + x * 100, map->_y + y * 100, rotation);
                monster->SetPositionX(
                    monster->GetPositionX() +
                    core->GetRandomNumber<int32_t>(-100, 100));
                monster->SetPositionY(
                    monster->GetPositionY() +
                    core->GetRandomNumber<int32_t>(-100, 100));
                map->_world->SpawnObject(monster);
                _instances[monster->GetVID()] = std::move(monster);
            }
        }
    }
}  // namespace game::environment