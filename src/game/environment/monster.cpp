// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "monster.hpp"

#include "../application.hpp"
#include "ai/simple_behaviour.hpp"

namespace game::environment {

    Monster::Monster(uint32_t id, uint32_t vid, int32_t x, int32_t y,
                     float rotation)
        : Object(vid, x, y, rotation),
          _spawnX(x),
          _spawnY(y),
          _proto(game::Application::GetInstance()->GetMonster(id)) {}

    Monster::~Monster() = default;

    void Monster::Show(
        const std::shared_ptr<core::networking::Connection>& connection) {
        auto monster =
            connection->GetServer()->GetPacketManager()->CreatePacket(
                0x01, core::networking::Outgoing);
        monster->SetField<uint32_t>("vid", _vid);
        monster->SetField<uint8_t>("characterType", GetObjectType());
        monster->SetField<float>("angle", GetRotation());
        monster->SetField<int32_t>("x", GetPositionX());
        monster->SetField<int32_t>("y", GetPositionY());
        monster->SetField<uint16_t>("class", _proto.id);
        monster->SetField<uint8_t>("moveSpeed", _proto.moveSpeed);
        monster->SetField<uint8_t>("attackSpeed", _proto.attackSpeed);
        connection->Send(monster);
    }

    void Monster::Remove(
        const std::shared_ptr<core::networking::Connection>& connection) {
        auto remove = connection->GetServer()->GetPacketManager()->CreatePacket(
            0x02, core::networking::Direction::Outgoing);
        remove->SetField("vid", GetVID());
        connection->Send(remove);
    }

    void Monster::Update(uint32_t elapsedTime) {
        if (_behaviour) _behaviour->Update(elapsedTime);

        Object::Update(elapsedTime);
    }

    void Monster::OnSpawned() {
        _behaviour =
            std::make_unique<ai::SimpleBehaviour>(Monster::shared_from_this());
        _behaviour->Init();
    }

    void Monster::ObjectEnteredView(std::shared_ptr<Object> object) {
        if (_behaviour) _behaviour->ObjectEnteredView(object);
    }

    void Monster::ObjectLeftView(std::shared_ptr<Object> object) {}
}  // namespace game::environment