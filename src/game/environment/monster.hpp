// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <memory>

#include "../formats/mob_proto.hpp"
#include "ai/behaviour.hpp"
#include "object.hpp"

namespace game::environment {
    class Monster : public Object,
                    public std::enable_shared_from_this<Monster> {
       public:
        Monster(uint32_t id, uint32_t vid, int32_t x = 0, int32_t y = 0,
                float rotation = 0);
        ~Monster() override;

        void Show(const std::shared_ptr<core::networking::Connection>&
                      connection) override;
        void Remove(const std::shared_ptr<core::networking::Connection>&
                        connection) override;
        void Update(uint32_t elapsedTime) override;
        std::shared_ptr<Object> GetPointer() override { return shared_from_this(); }

        void OnSpawned() override;
        void ObjectEnteredView(std::shared_ptr<Object> object) override;
        void ObjectLeftView(std::shared_ptr<Object> object) override;

        ObjectType GetObjectType() const override { return MONSTER; }
        const std::string& GetName() const override {
            return _proto.translatedName;
        }

        [[nodiscard]] int32_t GetSpawnX() const { return _spawnX; }
        [[nodiscard]] int32_t GetSpawnY() const { return _spawnY; }

        const formats::Monster& GetProto() const { return _proto; }

       private:
        const formats::Monster& _proto;
        int32_t _spawnX;
        int32_t _spawnY;

        std::unique_ptr<ai::Behaviour> _behaviour;
    };
}  // namespace game::environment