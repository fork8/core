// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#include "spawn_groups.hpp"

#include <cpptoml.h>
#include <vector>

#include "../../core/logger.hpp"

namespace game::formats {
    SpawnGroups::SpawnGroups() : _groups() {}

    SpawnGroups::~SpawnGroups() = default;

    bool SpawnGroups::Load(const std::string& path) {
        try {
            auto config = cpptoml::parse_file(path);

            auto groupArray = config->get_table_array("group");
            for (const auto& group : *groupArray) {
                SpawnGroup spawnGroup;

                auto id = group->get_as<uint64_t>("id");
                if (!id) {
                    CORE_LOGGING(error) << "Group id is missing or invalid!";
                    return false;
                }
                spawnGroup.id = *id;

                auto name = group->get_as<std::string>("name");
                if (name) {
                    spawnGroup.name = *name;
                }

                auto memberArray = group->get_table_array("member");
                for (const auto& member : *memberArray) {
                    auto memberId = member->get_as<uint32_t>("id");
                    if (!memberId) {
                        CORE_LOGGING(error)
                            << "Missing member id in group" << spawnGroup.id;
                        return false;
                    }
                    spawnGroup.members.push_back(*memberId);
                }

                CORE_LOGGING(trace) << "Loaded group " << spawnGroup.id << " ("
                                    << spawnGroup.name << ") with "
                                    << spawnGroup.members.size() << " members";
                _groups[spawnGroup.id] = std::move(spawnGroup);
            }

            return true;
        } catch (const cpptoml::parse_exception& e) {
            CORE_LOGGING(error) << "Failed to load spawn groups: " << e.what();
            return false;
        }
    }
}  // namespace game::formats