// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#ifdef ENABLE_ANGELSCRIPT
#include "../../../core/scripting/angelscript/angelscript.hpp"

namespace game::scripting::angelscript {
    class Interface {
       public:
        static void RegisterInterface(
            std::shared_ptr<core::scripting::angelscript::AngelScript> as);
    };
}  // namespace game::scripting::angelscript
#endif