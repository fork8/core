#include <game/item/inventory.hpp>

#include <gtest/gtest.h>

TEST(InventoryTests, TestBlocking) {
    // Create test inventory
    game::item::Inventory inventory(5, 5, 2);

    // Create test item with a size of 2
    game::formats::Item proto;
    proto.size = 2;
    auto item = std::make_shared<game::item::Item>(nullptr, proto, 0, 0);

    ASSERT_TRUE(inventory.Set(0, item));
    ASSERT_EQ(inventory.Get(0), item);

    ASSERT_FALSE(inventory.HasSpace(0, 1));
    ASSERT_FALSE(inventory.HasSpace(0, 2));
    ASSERT_FALSE(inventory.HasSpace(0, 3));
    ASSERT_FALSE(inventory.HasSpace(5, 1));
    ASSERT_FALSE(inventory.HasSpace(5, 2));
    ASSERT_FALSE(inventory.HasSpace(5, 3));
    ASSERT_TRUE(inventory.HasSpace(1, 1));
}